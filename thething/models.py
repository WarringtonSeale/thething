from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=512)
    dateadded = models.DateTimeField(auto_now=True)
    linkto = models.URLField()
    subject = models.CharField(max_length=128)
    description = models.TextField()
    semester = models.CharField(max_length=64)

    class Meta:
        ordering = ['dateadded',]

class Meme(models.Model):
    date = models.DateTimeField(auto_now=True)
    url = models.CharField(max_length=1024)
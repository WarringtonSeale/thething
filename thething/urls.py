from django.urls import path
from . import views

urlpatterns = [
    path('', views.thehomepage, name='themainpage'),
    path('thememepage/', views.thememepage, name='thememepage'),
]

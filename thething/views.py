from django.shortcuts import render
from thething.models import Post
from thething.models import Meme

# Create your views here.

def thehomepage(request):
    Postsqueryset = Post.objects.all().order_by('-dateadded')
    context = {
    'posts': Postsqueryset
    }
    return render(request, 'themainpage.html', context)

def thememepage(request):
    Memequeryset = Meme.objects.all
    context = {
        'memes': Memequeryset
        }
    return render(request, 'thememepage.html', context)